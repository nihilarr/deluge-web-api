<?php
/**
 * DelugeWebApi
 *
 * Simple library to interact with a Deluge server via webui.
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package   DelugeWebApi
 * @author    Drew Smith
 * @copyright copyright (c) 2018, Nihilarr (https://www.nihilarr.com)
 * @license	  http://opensource.org/licenses/MIT	MIT License
 * @link      https://gitlab.com/nihilarr/deluge-web-api
 * @version   0.0.1
 */

namespace Nihilarr;

/**
 * DelugeWebApi library
 */
class DelugeWebApi {
    /**
     *
     * @var resource $session
     */
    private $session;

    /**
     *
     * @var bool $authenticated
     */
    private $authenticated = false;

    /**
     *
     * @var string $host
     */
    private $host;

    /**
     *
     * @var string
     */
    private $pass;

    /**
     *
     * @var int $port
     */
    private $port;

    /**
     *
     * @var bool $use_ssl
     */
    private $use_ssl;

    /**
     *
     * @var string $request_uri
     */
    private $request_uri;

    /**
     *
     * @var string|null $error
     */
    private $error = null;

    /**
     *
     * @var int $errno
     */
    private $errno = 0;

    /**
     * Constructor
     *
     * @param string $host Host name or IP address of Deluge server.
     * @param string $pass Password to connect to Deluge server.
     * @param string $port Specifies the port number of the Deluge server.
     * @param bool $use_ssl True will connect using HTTPS, false HTTP.
     * @return object Returns object which represents the connection to a Deluge server
     */
    public function __construct(string $host = 'localhost', string $pass = 'deluge', int $port = 8112, bool $use_ssl = false) {
        $this->host = $host;
        $this->pass = $pass;
        $this->port = $port;
        $this->use_ssl = $use_ssl;
        $this->request_uri = ($use_ssl ? 'https' : 'http') . "://{$host}:{$port}/json";
        $this->session = curl_init($this->request_uri);
    }

    /**
     * Send HTTP request to Deluge server.
     * @param string $method The name of the remote method to call.
     * @param array|null $params The arguments to call the method with.
     * @return mixed The data requested
     */
    protected function request(string $method, array $params = null) {
        $response = false;

        $data = json_encode(array(
            'id' => uniqid(),
            'method' => $method,
            'params' => $params
        ));

        $options = array(
            CURLOPT_POST => 1,
            CURLOPT_HEADER => false,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => 'gzip',
            CURLOPT_CONNECTTIMEOUT => 30,
            CURLOPT_TIMEOUT => 400,
            CURLOPT_POSTFIELDS => $data
        );

        if($this->authenticated) {
            $options[CURLOPT_COOKIEFILE] = '';
        }
        else {
            $options[CURLOPT_COOKIEJAR] = '';
        }

        curl_setopt_array($this->session, $options);

        $http_response = json_decode(curl_exec($this->session), true);

        if(isset($http_response['result']) && $http_response['result']) {
            $response = $http_response['result'];
        }
        else {
            $this->error = curl_error($this->session);

            if($method == 'auth.login' && empty($this->error)) {
                $this->error = 'Authentication failed';
            }
        }

        return $response;
    }

    /**
     * Set the Deluge host. If $host is null, return the current host.
     * @param string $host Host
     * @return object|string
     */
    public function host(string $host = null) {
        if(!is_null($host)) {
            $this->host = $host;
            return $this;
        }
        return $this->host;
    }

    /**
     * Set the port number. If $port is null, return the current port number.
     * @param int $port Port number
     * @return object|int
     */
    public function port(int $port = null) {
        if(!is_null($port)) {
            $this->port = $port;
            return $this;
        }
        return $this->port;
    }

    /**
     * Toggle using HTTPS. If $use_ssl is null, return the current value.
     * @param bool $use_ssl True to use SSL, false to not
     * @return object|bool
     */
    public function use_ssl(bool $use_ssl = null) {
        if(!is_null($use_ssl)) {
            $this->use_ssl = $use_ssl;
            return $this;
        }
        return $this->use_ssl;
    }

    /**
     * Attempt to authenticate and return true on success, false if not.
     * @return bool Authentication status
     */
    public function authenticate() {
        if(!$this->authenticated) {
            $this->authenticated = $this->request('auth.login', array($this->pass));

            if(!$this->authenticated) {
                $this->error = "Could not authenticate";
                // trigger_error("Could not authenticate", E_USER_ERROR);
            }
        }
        return $this->authenticated;
    }

    /**
     * Get authentication status.
     * @return bool True if authenticated, false if not
     */
    public function authenticated() {
        return $this->authenticated;
    }


    /**
     * Connect the client to a daemon.
     * @param string $host_id ID of the daemon in the host list
     * @return array Array of methods the daemon supports
     */
    public function connect($host_id) {
        return $this->request('web.connect', array($host_id));
    }

    /**
     * The current connection state.
     * @return bool True if client is connected
     */
    public function connected() {
        return !!$this->request('web.connected', array());
    }

    /**
     * Disconnect the web interface from the connected daemon.
     * @return bool True on success, false on failure
     */
    public function disconnect() {
        return !!$this->request('web.disconnect', array());
    }

    /**
     * Gather the information required for updating the web interface.
     * @param array $keys Array of info keys about the torrents to gather
     * @param array $filters Assoc array of filters when selecting torrents
     * @return array The torrent and ui information
     */
    public function update_ui(array $keys = array(), array $filters = array()) {
        return $this->request('web.update_ui', array($keys, $filters));
    }

    /**
     * Get the entire config.
     * @return array Associative array of config values
     */
    public function get_config() {
        return $this->request('core.get_config', array());
    }

    /**
     * Get config value associated with $key.
     * @param string $key Key of config value to return
     * @return mixed Config value associated with $key
     */
    public function get_config_value(string $key) {
        return $this->request('core.get_config_value', array($key));
    }

    /**
     * Get all config values.
     * @param array $keys Array of config keys to be returned
     * @return array Config values
     */
    public function get_config_values(array $keys) {
        return $this->request('core.get_config_values', array($keys));
    }

    /**
     * Return array of all labels.
     * @return array Array of labels
     */
    public function get_labels() {
        return $this->request('label.get_labels', array());
    }

    /**
     * Add a label.
     * @param string $label_id Label to add
     * @return bool True on success, false on failure
     */
    public function add_label(string $label_id) {
        return !!$this->request('label.add', array($label_id));
    }

    /**
     * Remove a label.
     * @param  string $label_id Label to remove
     * @return bool True on success, false on failure
     */
    public function remove_label(string $torrent_id) {
        return !!$this->request('label.remove', array($label_id));
    }

    /**
     * Assign torrent a label, remove label if $label_id is null.
     * @param string $torrent_id ID of torrent to retrieve
     * @param string $label_id Label to assign to the torrent
     * @return bool True on success, false on failure
     */
    public function set_label(string $torrent_id, string $label_id = null) {
        return $this->request('label.set_torrent', array($torrent_id, $label_id));
    }

    /**
     * Return associative array of torrent key/value pairs.
     * @param string $torrent_id ID of torrent to retrieve
     * @param array $keys Array of keys to be returned
     * @return array Associative array of torrent key/value pairs
     */
    public function get_torrent(string $torrent_id, array $keys = array()) {
        $default_keys = array('files', 'hash', 'label', 'name', 'save_path', 'total_size');
        $keys = array_merge($default_keys, $keys);

        return $this->request('core.get_torrent_status', array($torrent_id, $keys));
    }

    /**
     * Return array of torrents.
     * @param array $filters Associative array of key/value pairs to filter results
     * @param array $keys Array of keys to be returned
     * @return array Array of torrents on success, false on failure
     */
    public function get_torrents(array $filters = array(), array $keys = array()) {
        $default_keys = array('files', 'hash', 'label', 'name', 'save_path', 'total_size');
        $keys = array_merge($default_keys, $keys);

        return $this->request('core.get_torrents_status', array($filters, $keys));
    }

    /**
     * Add torrents by file.
     * @param array $torrents Assoc array containing path of torrent file and options
     */
    public function add_torrents(array $torrents) {
        return $this->request('web.add_torrents', array($torrents));
    }

    /**
     * Gets array of files for a torrent.
     * @param string $torrent_id ID of the torrent to retrieve
     * @return array Array of files
     */
    public function get_torrent_files(string $torrent_id) {
        return $this->request('web.get_torrent_files', array($torrent_id));
    }

    /**
     * Return information about a torrent on the filesystem.
     * @param string $filename Path to the torrent
     * @return array Assoc array with info about torrent
     */
    public function get_torrent_info(string $filename) {
        return $this->request('web.get_torrent_info', array($filename));
    }

    /**
     * Pause torrents by torrent IDs.
     * @param array $torrent_ids Array of torrent IDs
     * @return bool True on success, false on failure
     */
    public function pause_torrent(array $torrent_ids) {
        return !!$this->request('core.pause_torrent', array($torrent_ids));
    }

    /**
     * Resume torrents by torrent IDs.
     * @param array $torrent_ids Array of torrent IDs
     * @return bool True on success, false on failure
     */
    public function resume_torrent(array $torrent_ids) {
        return !!$this->request('core.resume_torrent', array($torrent_ids));
    }

    /**
     * Pause all torrents in the session.
     * @return bool True on success, false on failure
     */
    public function pause_all_torrents() {
        return !!$this->request('core.pause_all_torrents', array());
    }

    /**
     * Resume all torrents in the session.
     * @return bool True on success, false on failure
     */
    public function resume_all_torrents() {
        return !!$this->request('core.resume_all_torrents', array());
    }

    /**
     * Enable Deluge plugin.
     * @param string $plugin Plugin name/id
     * @return bool True on success, false on failure
     */
    public function enable_plugin(string $plugin) {
        return !!$this->request('core.enable_plugin', array($plugin));
    }

    /**
     * Disable Deluge plugin.
     * @param string $plugin Plugin name/id
     * @return bool True on success, false on failure
     */
    public function disable_plugin(string $plugin) {
        return !!$this->request('core.disable_plugin', array($plugin));
    }

    /**
     * Returns an array of the exported methods.
     * @return array Array of exported methods
     */
    public function get_method_list() {
        return $this->request('daemon.get_method_list', array());
    }

    /**
     * Adds a host to the list.
     * @param string $host The host
     * @param int $port The port
     * @param string $username The username
     * @param string $password The password
     * @return bool True on success, false on failure
     */
    public function add_host($host, $port, $username, $password) {
        return !!$this->request('web.add_host', array($host, $port, $username, $password));
    }

    /**
     * Removes a host for the list.
     * @param string $host_id The hash id of the host
     * @return bool True on success, false on failure
     */
    public function remove_host($host_id) {
        return !!$this->request('web.remove_host', array($host_id));
    }
    
    /**
     * Return the hosts in the host list.
     * @return array Array of host objects
     */
    public function get_hosts() {
        return $this->request('web.get_hosts', array());
    }

    /**
     * Returns the current status for the specified host.
     * @param string $host_id The hash id of the host
     * @return array Array of host status fields
     */
    public function get_host_status($host_id) {
        return $this->request('web.get_host_status', array($host_id));
    }

    /**
     * Last error message reported.
     * @return string Error message
     */
    public function error() {
        return $this->error;
    }

    /**
     * Last error number reported.
     * @return string Error number
     */
    public function errno() {
        return $this->errno;
    }

    /**
     * Close Deluge connection/session.
     */
    public function close() {
        if($this->authenticated) {
            curl_close($this->session);
        }
    }

    /**
     * Destructor
     */
    public function __destruct() {
        $this->close();
    }
}
