
# deluge-web-api

> Library to access Deluge using the webui

## Requirements

* [PHP](https://secure.php.net/manual/en/install.php) >= 5.3.29

## Install

### Composer
```bash
composer require nihilarr/deluge-web-api
```
### Non-Composer
```php
require('DelugeWebApi.php'); // Require DelugeWebApi/DelugeWebApi.php file
```

## Usage

```php
use Nihilarr\DelugeWebApi;

$deluge = new DelugeWebApi('localhost', 'password');

if($deluge->authenticate()) {
    $torrents = $deluge->get_torrents(['label' => 'movies'], ['name']);

    var_dump($torrents);
}

# Array
# (
#   ["torrent-hash"] =>
#     Array
#     (
#       ["name"] => string(54) "Resident.Evil.Retribution.2012.720p.BRRip.x264-testing"
#     )
# )
```

### Note

## Contributing

## License

MIT © [Drew Smith](https://www.nihilarr.com)
